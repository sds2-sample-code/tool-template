

class ToolTemplateRunner(object):
    def __init__(self, *args, **kw):
        self._selected_strut = None
        self._factory_run = False
        self._bay_bounding_box = None

    @property
    def FactoryRun(self):
        """Indicate whether or not the tool is being run via modeling or
        from the factory method

        Note:
        -----
        If the tool is facotry run, skip the UI
        
        Returns:
            bool: True if run from the factory, False otherwise
        """

        return self._factory_run

    def Run(self):
        """Where the work is done
        """
        if not self.FactoryRun:
            # skip UI in this case
            pass

        pass

    @classmethod
    def Factory(cls):
        """Instantiate, populate, and return an object

        For use from another custom object. 
        
        Returns:
            ToolTemplate: An instantiated object with defaults filled
        """
        f = cls()
        # set relevant attributes here

        return f
