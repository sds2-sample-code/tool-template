import os

from Commands import AvailableTools
from Commands import CmdArgs
from Commands import Command
from Commands import CommandType
from Commands import Icon
from Commands import IconSet
from Commands import Listing
from Commands import OperationClass
from Commands import Station

class ToolTemplate(Command):
    exec_file = os.path.join(
        os.path.dirname(__file__),
        "ToolTemplate_runner.py"
    )

    UserStr = "User Visible Name"

    def __init__(self):
        Command.__init__(
            self,
            operations=OperationClass.Param,
            listings=Listing.All,
            icons=IconSet(tuple()),
            long_description="User Visible Name",
            command_text="""User Visible Name""",
            alt_text="""User Visible Name""",
            documentation="""""",
            clone=False,
            stations=Station.Frame | Station.FrameReview | Station.FrameBim | Station.FrameErector | Station.FrameErectorPlus | Station.FrameApproval | Station.FrameFabricator | Station.FrameModelstn | Station.FrameDraftstn | Station.FrameLite | Station.FrameErectorPlus,
            type=CommandType.Command
        )

    def Invoke(self, args):
        args = CmdArgs(parametric_file=self.exec_file)
        AvailableTools.param_run(args)
