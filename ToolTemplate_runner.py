import os
import sys

if os.path.dirname(__file__) not in sys.path:
    sys.path.append(os.path.dirname(__file__))

from ToolTemplate import ToolTemplateRunner

ToolTemplateRunner().Run()